[[_TOC_]]

## What is this project?

This is a template for new projects development. This is not yet read for production environments use it at your own risk.



## Installation

Clone this repo on your machine
Run:
```
  shell

virtualenv .venv
source .venv/bin/activate
pip install -e app

```


Running on development environment: `FLASK_APP=myapp:app flask run --reload -p 8080`
Running on production environment (not recommended): `gunicorn -w 4 -b :8080 wsgi:app`