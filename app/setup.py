from setuptools import find_packages, setup

setup (
    name='myapp',
    version='1.0.0',
    packages=find_packages(),
    include_package_data=True,
    zip_safe=False,
    install_requires=[
        'Flask-Compress==1.13',
        'Flask==2.2.2',
        'gunicorn==20.1.0',
        'python-dotenv==0.21.1'
    ]
)
