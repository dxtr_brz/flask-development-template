import datetime
import os

from flask import Flask, jsonify, make_response, render_template, request


def init_app(app: Flask):

    @app.route('/', methods=['GET', 'POST'])
    def root():
        name = os.environ.get("MY_NAME")
        print(name)
        if request.method == 'GET':
            return render_template('index.html', my_name=name, utc_dt=datetime.datetime.utcnow()), 200
        if request.method == 'POST':
            return make_response(jsonify({"myname": name}), 200)

    @app.route('/about/')
    def about():
        return render_template('about.html')


    @app.errorhandler(404)
    def not_found(err):
        if request.method == 'GET':
            return render_template("not_found.html"), 404
        if request.method == 'POST':
            return make_response(jsonify({'error': 'Not found'}), 404)
            