from ext import mylib
from views import myview

from flask import Flask
from flask_compress import Compress

app = Flask(__name__)
Compress(app)

mylib.init_app(app)
myview.init_app(app)

if __name__ == '__main__':
    app.run(host='0.0.0.0', debug=True, ssl_context="adhoc")
